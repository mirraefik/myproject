package Menu;
import java.util.*;

public class Que{ //Que - Question
  ArrayList<Que> children = new ArrayList<Que>();
  Que parent;
  String descriptionToParent = "Следующий уровень";
  String description = "Вы находитесь в меню. Выберите вариант:";
  ArrayList<Action> actions = new ArrayList<Action>();
  Que(){

  }

  Que(String description){
    this.description = description;
  }


  public void setDescription(String description){
    this.descriptionToParent = description;
  }

  public void printDescription(){
    System.out.println(this.description);
    if (this.parent != null) {
      System.out.println("0. Вернуться назад");
    } else {
      System.out.println("0. Выход");
    }
    int num = 1;
    for (int i = 0; i < this.actions.size(); i++) {
      System.out.println("" + num + ". " + this.actions.get(i).getDescription());
      num += 1;
    }
    for (int i = 0; i < this.children.size(); i++) {
      System.out.println("" + num + ". " + this.children.get(i).descriptionToParent);
      num += 1;
    }
    System.out.println();
  }

  public void setDescriptionToParent(String descriptionToParent) {
    this.descriptionToParent = descriptionToParent;
  }

  public void setAction(Action action){
    this.actions.add(action);
  }

  public void connect(Que nextQue){
    this.children.add(nextQue);
    nextQue.parent = this;
  }
}
