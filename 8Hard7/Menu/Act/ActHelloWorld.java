package Menu.Act;


public class ActHelloWorld extends Act{

  public ActionPrintHelloWorld(String description) {
    super(description);
  }

  public void act(){
    System.out.println("Hello, World!");
  }
}
