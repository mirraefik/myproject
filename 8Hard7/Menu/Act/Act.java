package Menu;

abstract public class Act{
  String description;

  protected Action(String description){
    this.description = description;
  }

  abstract public void act();

  public String getDescription(){
    return this.description;
  }
}
