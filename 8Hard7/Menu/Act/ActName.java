package Menu.Act;

import java.util.Scanner;

public class ActName extends Act{
  String description = "Вывести имя";

  public ActionPrintName(String description) {
    super(description);
  }

  public void act(){
    Scanner scan = new Scanner(System.in);
    String name = scan.next();
    System.out.println("\n Hello, "  + name + "! \n");
  }
}
