package Menu;
import java.util.*;

class Main{
  public static void main(String[] args) {
    Que zero = new Que();
    Que first = new Que();
    Que second = new Que();
    Act hello = new ActHelloWorld("Вывести Hello, world");
    zero.connect(first);
    zero.setAct(hello);

    first.setAct(new ActName("Вывести имя"));
    first.connect(second);

    second.setAct(new ActName("Вывести дату"));

    menuStart(zero);
  }

  public static void menuStart(Que start) {
    Scanner scan = new Scanner(System.in);
    int answ = 0;
    boolean flag = true;

    while(flag){
      start.printDescription();
      answ = scan.nextInt();
      if (answ == 0){
         if (start.parent == null){
              flag = false;
        } else {
          start = start.parent;
        }
      }
      if(answ > 0 && answ <= start.actions.size()){
        start.actions.get(answ - 1).act();
        }
      if(answ > start.actions.size() && answ <= start.actions.size() + start.children.size()) {
          start = start.children.get(answ - start.children.size());
        }
      if(answ > start.actions.size() + start.children.size()){
          System.out.println("Введите корректное значение");
        }
    }
  }
}
