#!/usr/bin/bash
#создание файла для данных
> data
exec 6>&1
exec 1>data
#ввод данных
read INPUT
q=0
#пишем время когда начинаем
VARStart="$(date)"
#запускаем цикл
while [[ $q -lt "2**$INPUT" ]]
do 
echo $(( 2**$q ))
let $(( q=$q+1 ))
echo $q
done
#пишем время конца
VAR="$(date)"
echo "$VARStart"
echo "$VAR"
#выход из файла
exec 1>&-
exec 1>&6
exec 6>&-
#вывод в строку
cat data 
