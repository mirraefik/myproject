package Sort;

class Sort{

   // первый метод сортирует массив изменяя его
   public static void sort(double[] arr){
     for (int i = 0; i < arr.length; i++) {
         int min = arr[i];
         int mini = i;
         for (int j = i+1; j < arr.length; j++) {
             if (arr[j] < min) {
                 min = arr[j];
                 mini = j;
             }
         }
      }
    }


    //второй метод изменяет массив, создает еще один и записывает измененный массив в новосозданный
    public static double[] createSortedArray(double[] arr) {
      double[] newarr = arr.clone();
      for (int i = 0; i < newarr.length; i++) {
          int min = newarr[i];
          int mini = i;
          for (int j = i+1; j < newarr.length; j++) {
              if (newarr[j] < min) {
                  min = newarr[j];
                  mini = j;
              }
          }
      return newarr;
    }
  }
}
