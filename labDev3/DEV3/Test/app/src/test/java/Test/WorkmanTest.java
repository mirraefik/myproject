package Workman;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class WorkmanTest {
  Workman workman = new Workman("Denis", "workman", 50000);

  @Test void tryWorkmanWork() {
    assertEquals("Я работаю!", workman.work(), "tryWorkmanWork is incorrect");
  }
}
