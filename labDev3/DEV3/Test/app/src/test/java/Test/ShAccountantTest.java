package ShAccountantTest;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class HeadTest {
  ShAccountant head = new ShAccountant("Anthony", "ShAccountant", 50000);

  @Test void tryShCountAccSalary() {
    assertEquals("Я дал нашим бухгалтерам зарплату!", head.countAccSalary(), "tryShCountAccSalary is incorrect");
  }
}
