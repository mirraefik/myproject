package AccTest;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AccTest {
  Acc acc = new Acc("Tony", "Accountant", 50000);

  @Test void tryAccGiveSalary() {
    assertEquals("Я что-то посчитал!", acc.giveSalary(), "tryAccGiveSalary is incorrect");
  }
}
