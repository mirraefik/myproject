package Test;

/*Класс Object сам по себе не реализует интерфейс Cloneable,
поэтому вызов метода clone для объекта, класс которого является Object,
приведет к возникновению исключения во время выполнения.*/
public class Worker implements Cloneable{
  public String name;
  public String position;
  public int salary;

  Worker(String name, String position, int salary){
    this.name = name;
    this.position = position;
    this.salary = salary;
  }

  public String getName(){
    return this.name;
  }

  public void setName(String name){
    this.name = name;
}

  public String getPosition(){
    return this.position;
  }

  public void setPosition(String position){
    this.position = position;
  }

  public int getSalary(){
    return this.salary;
  }

  public void setSalary(int salary){
    this.salary = salary;
  }

//переопределение методов Object
  public String toString(){  //метод toString возвращает строку, которая "текстуально представляет" этот объект
    return "Employee's name: " + this.name +
           ", position: " + this.position +
           ", salary: " + this.salary;
  }

  protected int myCipher(String a){ //для метода hashCode
    int d[] = new int[255];
    for (int i = 0; i < a.length(); i++) {
        d[i] = Integer.valueOf(a.charAt(i))*(100 - i);
    }
    int cypher = 0;
    for (int i = 0; i < d.length; i++){
      cypher += d[i];
    }
    return cypher;
  }

  public int hashCode() {  //Возвращает значение хэш-кода для объекта
    return myCipher(this.name) + myCipher(this.position);
  }

  public boolean equals(Object x) {
    /*Это рефлексивно: для любого ненулевого значения ссылки x функция x.equals(x) должна возвращать true.
    для любых ненулевых ссылочных значений x и y x.equals(y) должен возвращать true тогда и только тогда, когда y.equals(x) возвращает true.
    этот метод возвращает true тогда и только тогда, когда x и y относятся к одному и тому же объекту (x == y имеет значение true*/
    if(this == x && this != null && x != null) {
      return true;
    }
    //Для любого ненулевого ссылочного значения x функция x.equals(null) должна возвращать false.
    if(x == null || getClass() != x.getClass()){
      return false;
    }
    Worker worker = (Worker) x;
    return  this.salary == worker.salary &&
            this.position == worker.position &&
            this.name == worker.name;
  }

  public Object clone() throws CloneNotSupportedException { //Создает и возвращает копию этого объекта.
    return (Worker) super.clone(); // По соглашению возвращаемый объект должен быть получен вызовом super.clone. Если класс и все его суперклассы (кроме Object) подчиняются этому соглашению, это будет случай, когда x.clone().getClass() == x.getClass().
  }
}
